class User {    
  constructor(user) {
    this._id = user.id;
    this._username = user.username;
    this._password = user.password;
    this._name  = user.name;
    this._bio = user.bio;
  }

  get tweets() {
    return Math.floor(Math.random() * 1000);
  }

  get following() {
    return Math.floor(Math.random() * 1000);
  }

  get followers() {
    return Math.floor(Math.random() * 1000);
  }

  get username() {
    return `@${this._username}`;
  }

  get link() {
    return `/${this._username}`; 
  }
  
  get name() {
    return this._name;
  }
  
  get bio() {
    return this._bio;
  }
  
  get password() {
    return this._password;
  }
}