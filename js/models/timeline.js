class Timeline {  
  constructor() {
      this._tweets = [];
  }
  
  novoTweet(tweet) {
      this._tweets.push(tweet);
  }
  
  get tweets() {
    return [].concat(this._tweets);
  }
}