class Tweet {
  constructor(tweet) {
    this._id = tweet.id;
    this._content = tweet.tweet;
    if(tweet.user){
      this._user = new User(tweet.user);
    }
  }
  
  get content() {    
    return this._content;
  }

  get retweets() {
    return Math.floor(Math.random() * 1000);
  }
  
  get comments() {
    return Math.floor(Math.random() * 1000);
  }

  get likes() {
    return Math.floor(Math.random() * 1000);
  }

  get user() {
    return this._user;
  }
}