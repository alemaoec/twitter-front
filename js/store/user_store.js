class UserStorage extends View {
  static saveUser(user) {
    sessionStorage['user'] = JSON.stringify(user);
  }

  static retrieveUser() {
    if(!sessionStorage['user']){
      return undefined;
    }
    return JSON.parse(sessionStorage['user']);
  }
}
