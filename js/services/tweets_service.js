class TweetsService {
  getTimeline(callBack) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:8080/tweets');
    xhr.onreadystatechange = () => {
      if(xhr.readyState == 4) {
        if(xhr.status == 200) {   
          let response = JSON.parse(xhr.responseText).map( function(objeto) {
            return new Tweet(objeto);  
          }); 
          callBack(null, response);  
        } else {
          console.log(xhr.responseText);
          callBack('Não foi possível obter os tweets', null);
        }
      }
    };
    xhr.send();
  }

  createNewTweet(tweet, callBack) {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost:8080/tweets', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = () => {
      if(xhr.readyState == 4) {
        if(xhr.status == 200) {   
          let response = new Tweet(JSON.parse(xhr.responseText));
          callBack(null, response);  
        } else {
          console.log(xhr.responseText);
          callBack('Não foi possível criar o tweet', null);
        }
      }
    };
    console.log("asdada", tweet);
    xhr.send(JSON.stringify(tweet));
  }    
}