class UsersService {
  getUserDetails(callBack) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:8080/me');
    xhr.onreadystatechange = () => {
      if(xhr.readyState == 4) {
        if(xhr.status == 200) {   
          let response = new User(JSON.parse(xhr.responseText));  
          UserStorage.saveUser(response);
          callBack(null, response);  
        } else {
          console.log(xhr.responseText);
          callBack('Não foi possível obter os dados do usuário', null);
        }
      }
    };
    xhr.send();
  }
}