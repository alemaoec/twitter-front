class TimelineController {
  constructor() {
    this._timeline = new Timeline()
    this._tweetService = new TweetsService();
    this._userService = new UsersService();

    this._tweetValue = document.querySelector('#tweet-value')

    this._tweetView = new TweetView(document.querySelector('#timeline-content'));
    this._userView = new UserView(document.querySelector('#user-content'));

    this.carregaTimeline()
  }

  adiciona(evento) {
    evento.preventDefault();
    this._tweetService.createNewTweet(this._criaTweet(), (erro, tweet) => {
      this._timeline.novoTweet(tweet);
      this._tweetView.render(tweet);
      this._tweetValue.value = '';      
      $("#new-tweet").modal('hide')
    });
  }

  _criaTweet() {
    return {"tweet": this._tweetValue.value};    
  }

  carregaTimeline() {
    // let self = this;
    // this._service.getTimeline((erro, tweets) => {
    //   tweets.forEach(function(tweet) {
    //     self._timeline.novoTweet(tweet);
    //   });
    // });
    this._userService.getUserDetails((erro, user) => {
      this._userView.render(user);
    });

    this._tweetService.getTimeline((erro, tweets) => {
      tweets.forEach( (tweet) => {
        this._timeline.novoTweet(tweet);
        this._tweetView.render(tweet);
      });
    });
    console.log("Timeline", this._timeline)
  }
}