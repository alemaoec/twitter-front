class UserView extends View {
  constructor(elemento) {
    super(elemento);
  }
    
  _template(model) {
    return `
    <div class="profile-card">
      <a class="profile-background" href="${model.link}" tabindex="-1" aria-hidden="true" rel="noopener"></a>
      <div class="account-group">
        <div class="screen-name">
          <a class="screen-name-link" href="${model.link}">${model.name}</a>
        </div>
        <span>
          <a class="screen-name-link" href="${model.link}">
            <span class="username" dir="ltr">
              ${model.username}
            </span>
          </a>
        </span>
      </div>

      <div class="status-card">
        <ul class="status-list">
          <li class="status-item">
            <a class="status-link" href="${model.link}">
              <span class="label">Tweets</span>
              <span class="value">${model.tweets}</span>
            </a>
          </li>
          <li class="status-item">
            <a class="status-link" href="${model.link}">
              <span class="label">Following</span>
              <span class="value">${model.following}</span>
            </a>
          </li>
          <li class="status-item">
            <a class="status-link" href="${model.link}">
              <span class="label">Followers</span>
              <span class="value">${model.followers}</span>
            </a>
          </li> 
        </ul>
      </div>
    </div>
    `;
  }
}
