class View {
  constructor(elemento) {
    this._elemento = elemento;
  }

  render(model) {
    this._elemento.prepend(this._htmlToElement(this._template(model)));
  }

  _htmlToElement(html) {
    let template = document.createElement('template');
    html = html.trim();
    template.innerHTML = html;
    return template.content.firstChild;
  }
}